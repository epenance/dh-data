import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Team } from './teams.entity';

@Entity({
  name: 'players',
})
export class Player {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int')
  active: number;

  @Column('varchar')
  fullname: string;

  @Column('varchar')
  nickname: string;

  @Column('varchar')
  image: string;

  @Column('varchar')
  country: string;

  @Column('varchar')
  twitter: string;

  @Column('varchar')
  command: string;

  @Column('int')
  team_id: number;

  @Column('int')
  position: number;

  @ManyToOne(type => Team, team => team.players)
  @JoinColumn({ name: 'team_id' })
  team: Team;

}