import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn } from 'typeorm';
import { Player } from './players.entity';

@Entity({
  name: 'teams',
})
export class Team {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int')
  active: number;

  @Column('varchar', {name: 'TeamName'})
  teamName: string;

  @Column('varchar')
  short: string;

  @Column('varchar')
  tag: string;

  @Column('varchar', {name: 'image'})
  teamLogo: string;

  @Column('varchar')
  countryteam: string;

  @Column('varchar')
  command: string;

  @OneToMany(type => Player, player => player.team, {eager: true})
  players: Player[];
}