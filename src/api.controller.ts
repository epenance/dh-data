import { Get, Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Team } from './models/teams.entity';
import { Repository } from 'typeorm';
import { Player } from './models/players.entity';

@Controller()
export class ApiController {
  constructor(@InjectRepository(Team)
              private readonly teamRepository: Repository<Team>,
              @InjectRepository(Player)
              private readonly playerRepository: Repository<Player>,
  ) {}

  @Get('teams')
  teams() {
    return this.teamRepository.find({ where: {
        active: 1,
      },
    order: {
      teamName: 'ASC',
    }});
  }

  @Get('players')
  players() {
    return this.playerRepository.find({ where: {
        active: 1,
      },
    order: {
      fullname: 'ASC',
    }});
  }
}
