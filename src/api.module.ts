import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Team } from './models/teams.entity';
import { ApiController } from './api.controller';
import { Player } from './models/players.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Team, Player]),
  ],
  controllers: [ApiController],
  providers: [],
})
export class ApiModule {}
